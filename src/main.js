import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
import { Tooltip, Loading, Upload, Drawer, ColorPicker, Slider } from 'element-ui'
import lang from 'element-ui/lib/locale/lang/vi'
import locale from 'element-ui/lib/locale'
import 'element-ui/lib/theme-chalk/index.css'
import HoneybadgerVue from '@honeybadger-io/vue'
import { honeyBadgerConfig } from '@/api/constant.js'
import { isMobile } from '@/utils/common.js'
import toastObject from '@/utils/toast.js'

Vue.config.productionTip = false
Vue.prototype.$axios = axios
Vue.prototype.$isMobile = isMobile
Vue.prototype.$toast = toastObject

Vue.use(VueSweetalert2)

Tooltip.props.enterable.default = false
Vue.use(Tooltip)
Vue.use(Loading)
Vue.use(Upload)
Vue.use(Drawer)
Vue.use(ColorPicker)
Vue.use(Slider)
locale.use(lang)

Vue.use(HoneybadgerVue, honeyBadgerConfig)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
