import firebase from 'firebase/app';
import 'firebase/firestore';
import { firebaseConfig } from './constant.js';

export const firebaseInit = () => {
    if (firebase.apps.length === 0) {
        firebase.initializeApp(firebaseConfig);
    }
    return firebase.firestore();
}