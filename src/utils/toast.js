import Vue from 'vue'

export default {
    success: (options, options2) => {
        let swalOptions = options
        if(typeof options == 'string'){
            swalOptions = { text: options }
            options = options2 ?? {}
        }
        swalOptions.showConfirmButton = false
        swalOptions.timerProgressBar = true
        swalOptions.toast = options.toast ?? true
        swalOptions.timer = options.timer ?? 1000
        swalOptions.position = options.position ?? 'top-end'
        swalOptions.icon = 'success'
        
        Vue.swal.fire(swalOptions)
    },
    info: (options, options2) => {
        let swalOptions = options
        if(typeof options == 'string'){
            swalOptions = { text: options }
            options = options2 ?? {}
        }
        swalOptions.showConfirmButton = false
        swalOptions.timerProgressBar = true
        swalOptions.toast = options.toast ?? true
        swalOptions.timer = options.timer ?? 1000
        swalOptions.position = options.position ?? 'top-end'
        swalOptions.icon = 'info'
        
        Vue.swal.fire(swalOptions)
    },
    warning: (options, options2) => {
        let swalOptions = options
        if(typeof options == 'string'){
            swalOptions = { text: options }
            options = options2 ?? {}
        }
        swalOptions.showConfirmButton = false
        swalOptions.timerProgressBar = true
        swalOptions.toast = options.toast ?? true
        swalOptions.timer = options.timer ?? 1000
        swalOptions.position = options.position ?? 'top-end'
        swalOptions.icon = 'warning'
        
        Vue.swal.fire(swalOptions)
    },
    error: (options, options2) => {
        let swalOptions = options
        if(typeof options == 'string'){
            swalOptions = { text: options }
            options = options2 ?? {}
        }
        swalOptions.showConfirmButton = false
        swalOptions.timerProgressBar = true
        swalOptions.toast = options.toast ?? true
        swalOptions.timer = options.timer ?? 1000
        swalOptions.position = options.position ?? 'top-end'
        swalOptions.icon = 'error'
        
        Vue.swal.fire(swalOptions)
    },
}