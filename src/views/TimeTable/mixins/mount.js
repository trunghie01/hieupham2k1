import Vue from 'vue';
import Panel from '../Panel.vue';

export default {
	components: {
        Panel,
	},
	methods: {
		newPanel(top, left, classInfo, level){
            var ComponentClass = Vue.extend(Panel)
			 // error when TKB excel has change column order or add more column
            // classInfo.color = classInfo[26] // classInfo is an index array
            var instance = new ComponentClass({
                propsData: {
                    top, left, classInfo,
                    title: this.title,
                    titleTrans: this.titleTrans,
                    index: this.chosenClasses.length,
                    level: level,
                    rawTitle: this.rawTitle,
                    panelRatio: this.panelRatio,
                }
            })
            instance.$on('deletePanel', e => {
                this.deletePanel(e.index, e.day)
            })
            instance.$on('changeColor', e => {
                classInfo.color = e.color
                instance.$forceUpdate()
            })
			instance.$on('addClass2', e => {
				if (this.jsonTable?.[e.subjectCode]?.[e.classCode]) {
					this.chooseClass(this.jsonTable[e.subjectCode][e.classCode])
				}
            })
            instance.$mount()

			if (this.$refs.table) {
				this.$refs.table.appendChild(instance.$el)
			}

            return instance
        },
		deletePanel(index, day) {
            if(this.isDoneScheduling()) return this.$toast.info('Lịch đã cố định, vui lòng lưu lại mã lớp và TKB.')

			if (this.$refs.table) {
				this.$refs.table.removeChild(this.chosenClasses[index].$el)
			}

            this.chosenClasses[index].$destroy()
            this.chosenClasses[index] = undefined // must not splice to keep other indexes

            // delete in layer
            let panelDownLeveled = []
            for(let time in this.layer[day]){
                let layer = this.layer[day][time]
                let foundLayer = false
                for(let i = 0; i < layer.length; i++){
                    if(layer[i] == index){
                        layer.splice(i, 1)
                        foundLayer = true
                    }
                    if(foundLayer && !panelDownLeveled[layer[i]] && this.chosenClasses[layer[i]]){
                        if(this.chosenClasses[layer[i]].level > 0) this.chosenClasses[layer[i]].level--
                        panelDownLeveled[layer[i]] = true
                    }
                }
            }
            this.$forceUpdate()
        },
	}
}