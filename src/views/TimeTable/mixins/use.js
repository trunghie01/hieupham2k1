export default {
	data() {
		return {
			getSugestionTimeOut: null,
			tableHeight: 5,
		}
	},
	methods: {
		addSubjectToRegisterList() { // if input subject then upload TKB will alert not find subject
            let subjectCode = this.$refs['subjectCode'].value

            if(subjectCode.length > 8){ // for multi copy
                let rawSplit = subjectCode.split('\t')
                let subjects = rawSplit.filter(val => {
                    let str = val.trim().replaceAll('\n', '')
                    return 4 < str.length && str.length < 8 && /[A-Z]+/.test(str) && /[0-9]/.test(str)
                })
                let hasPush = false
                subjects.forEach(val => {
                    let subject = val.trim()
                    let duplicate = this.subjectCodes.find(val => val === subject)
                    if(subject.length > 0 && !duplicate){
                        this.subjectCodes.push(subject)
                        hasPush = true
                    }
                })

                let subjectMapCount = subjects.length
                for(let i = 0; i < this.rawTable.length; i++){
                    let tableSubjectCode = this.getRowInfo(this.rawTable[i], 'subjectCode')
                    if(this.rawTable[i] && tableSubjectCode && subjects.find(val => val.trim() == tableSubjectCode) && !this.subjectNameMap[tableSubjectCode]){
                        this.subjectNameMap[tableSubjectCode] = this.getRowInfo(this.rawTable[i], 'subjectName')
                        if(--subjectMapCount == 0) break
                    }
                }

                if(hasPush) this.$toast.success('Đã thêm các Mã HP bạn mong muốn')
                else this.$toast.error('Không lọc được thêm Mã HP nào')
            } else {
                subjectCode = subjectCode.replaceAll(' ', '')
                if(subjectCode != ''){
                    if(subjectCode.length < 6) return // no toast alert because when blur to click on sugest subject, this function will be called twice
                    if(this.sugestSubjects.length == 0) return this.$toast.error('Không có Mã học phần này trong TKB')
                }
                let duplicate = this.subjectCodes.find(val => val === subjectCode) // why not use indexOf?
                if(subjectCode.length > 0 && !duplicate){
                    this.subjectCodes.push(subjectCode)
                    if(this.currentSugestSubject == -1) this.currentSugestSubject = 0
                    if(this.sugestSubjects[this.currentSugestSubject]) this.subjectNameMap[subjectCode] = this.getRowInfo(this.sugestSubjects[this.currentSugestSubject], 'subjectName')
                }
            }
            this.$refs['subjectCode'].value = ''
            this.sugestSubjects = []
            this.currentSugestSubject = -1
        },
		getSugestion(event) {
			this.sugestSubjects = []
			event.target.value = event.target.value.toUpperCase().trimStart()

			if(event.target.value == '' || this.rawTable.length == 0) return
			this.currentSugestSubject = -1

			function removeAccents(str) {
				return str.normalize('NFD').replace(/[\u0300-\u036f]/g, '').replace(/đ/g, 'd').replace(/Đ/g, 'D');
			}

			if(this.getSugestionTimeOut) {
				clearTimeout(this.getSugestionTimeOut)
			}
			this.getSugestionTimeOut = setTimeout(() => {

				let hasPushed = {}
				let queryString = event.target.value
				let queryStringNoAccents = removeAccents(queryString)
				let subjectNameTitle = this.rawTitle[this.title[this.titleTrans['subjectName']]]?.toUpperCase()
				let sugestSubjectRooms = []

				for(let i = 0; i < this.rawTable.length; i++){
					let subjectCode = this.getRowInfo(this.rawTable[i], 'subjectCode')
					let subjectName = this.getRowInfo(this.rawTable[i], 'subjectName').toUpperCase()

					if(!hasPushed[subjectCode] && this.rawTable[i]){
						if(subjectCode.startsWith(queryString)){

							this.sugestSubjects.unshift(this.rawTable[i])
							hasPushed[subjectCode] = true

						} else if(subjectName){
							let queryStringIndex = removeAccents(subjectName).indexOf(queryStringNoAccents)

							if(queryStringIndex != -1 && subjectName != subjectNameTitle){ // if subjectName != Ten_HP

								if(!sugestSubjectRooms[queryStringIndex]) sugestSubjectRooms[queryStringIndex] = []
								sugestSubjectRooms[queryStringIndex].push(this.rawTable[i])
								hasPushed[subjectCode] = true
							}
						}
					}
				}

				for(let room of sugestSubjectRooms) {
					if(room) {
						this.sugestSubjects = [...this.sugestSubjects, ...room]
					}
				}
			}, 100)
        },
		chooseSubject(subjectCode, subjectName) {
            if(this.isDoneScheduling()) return this.$toast.info('Lịch đã cố định, vui lòng lưu lại mã lớp và TKB.')
            this.isChoosing = {}
            if(this.choosingSubject == subjectCode) {
                this.choosingSubject = null
                return this.$swal.close()
            }
            this.drawer = false
            this.choosingSubject = subjectCode

            for(let classCode in this.jsonTable[subjectCode]){
                if(classCode != 'subjectName'){
                    let classes = this.jsonTable[subjectCode][classCode] // why array? => because 1 class can be in many days, or many rooms
                    for(let key in classes){
                        if('classType' == key) continue
                        let classDay = this.getRowInfo(classes[key], 'day')
                        if(!this.isChoosing[classDay]) this.isChoosing[classDay] = {}

                        for(let time of this.tableTime){
                            if(classes[key].start <= time && time < classes[key].end){
                                if(!this.isChoosing[classDay][time]) this.isChoosing[classDay][time] = []
                                this.isChoosing[classDay][time].push(classes[key])
                            }
                        }
                    }
                }
            }
            this.$forceUpdate()
        },
        chooseClass(classInfos) {
            if(!classInfos/* || classInfos[0].length < this.rawTitle.length*/) return
            if(classInfos.length > 1){ // show modal to choose from overlay class
                this.overlayClasses = classInfos

				$('#selectClassModal').modal('show')
                return
            }
            const classInfo = classInfos[0]
            const anchorPadding = 15
            this.isChoosing = {}
            for(let i in this.chosenClasses){
                let chosenClassInfo = this.chosenClasses[i]?.classInfo
                if(!chosenClassInfo) continue
                if(this.getRowInfo(chosenClassInfo, 'classCode') == this.getRowInfo(classInfo, 'classCode') &&
                    this.getRowInfo(chosenClassInfo, 'day') == this.getRowInfo(classInfo, 'day') &&
                    chosenClassInfo.start == classInfo.start){
                        this.choosingSubject = null
                        return this.$toast.info('Lớp này đã có trong bảng')
                }
            }

            let classDay = this.getRowInfo(classInfo, 'day')
            let layerLevel = this.layer[classDay]?.[classInfo.start] ? this.layer[classDay][classInfo.start].length : 0
            // parseInt to avoid string concat
            let anchor = $('#anchor')
            let top = anchor.height() + parseInt(classInfo.start - this.tableTime[0])  / this.panelRatio
            let left = (this.getRowInfo(classInfo, 'day') - 1) * (anchor.width() + anchorPadding * 2) - anchorPadding * 2 - anchor.width() / 2

            this.chosenClasses.push(this.newPanel(top, left, classInfo, layerLevel))
            if(!this.layer[classDay]) this.layer[classDay] = {}
            for(let time of this.tableTime){
                if(classInfo.start <= time && time < classInfo.end){
                    if(!this.layer[classDay][time]) this.layer[classDay][time] = []
                    this.layer[classDay][time].push(this.chosenClasses.length - 1)
                }
            }
            this.$swal.close()
            this.choosingSubject = null

			$('#selectClassModal').modal('hide')
        },
		changeTableHeight() {
			this.panelRatio = 10 / this.tableHeight
			localStorage.setItem('tableHeight', this.tableHeight)
			this.reAddTable()
		},
		initTableHeight() {
			this.tableHeight = localStorage.getItem('tableHeight') ? parseInt(localStorage.getItem('tableHeight')) : 5
			this.panelRatio = 10 / this.tableHeight
		},
	}
  }