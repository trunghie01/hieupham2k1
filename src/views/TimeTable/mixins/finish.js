import XLSX from 'xlsx';

export default {
	data() {
		return {
			weeksMap: {},
		}
	},
	methods: {
		done() {
            if(this.chosenClasses.filter(val => val).length == 0) return this.$toast.error('Chưa có lớp nào trong bảng')

            if(this.isDoneScheduling()) return this.$toast.info('Lịch đã cố định, vui lòng lưu lại mã lớp và TKB.')

            let hasLayer = false
            let duplicate = ''
            for(let day in this.layer){
                for(let time in this.layer[day]){
					let layerClasses = this.layer[day][time].filter(val => val !== undefined)
                    if(layerClasses.length > 1 && this.isClassesOverlapWeek(layerClasses)){
                        hasLayer = true
                        duplicate += `Thứ ${day} lúc ${time}, `
                    }
                }
            }
            let  result = ''
			let tempTable = []
            if(!hasLayer) {
				tempTable = this.rawTable
				this.rawTable = [] // why erase rawTable => to push classInfo for filterSubject
			}
            this.chosenClasses.forEach((val, index) => {
                if(!val) return
                let classInfo = val.classInfo
				let classCode = this.getRowInfo(classInfo, 'classCode')
				let className = this.getRowInfo(classInfo, 'subjectName')
				let classType = this.getRowInfo(classInfo, 'classType')

                result += `<span title='${className} (${classType})'>${classCode}</span>`
				if (index < this.chosenClasses.length - 1) result += ', '

                if(!hasLayer){
                    this.rawTable.push(classInfo)

                    this.deletePanel(index, this.getRowInfo(classInfo, 'day')) // delete panel to clean up spaces in the schedule, re-add after then
                }
            })
            if(!hasLayer){
				this.subjectCodes = this.rawTable.map(val => this.getRowInfo(val, 'subjectCode'))
				this.subjectCodes = [...new Set(this.subjectCodes)] // remove duplicate

                this.filterSubject() // clean up spaces in the schedule, after cleanup, you cant edit because there might be no space to add class

                this.rawTable.forEach(val => this.chooseClass([val]))
				this.rawTable = tempTable
            } else {
                result += "(Có lớp trùng lịch vào " + duplicate + ")"
            }
            if(!this.$refs['result']) return
            if(result.length > 0) this.$refs['result'].innerHTML = 'Mã lớp đã chọn là: ' + result
            else this.$refs['result'].innerHTML = ''
        },
		exportFile() {
            let result = [this.rawTitle]
            this.chosenClasses.forEach(val => {
                if(val) {
                    val.classInfo.start = undefined // when get color from classInfo[26] will get classInfo.start
                    val.classInfo.end = undefined
                    // val.classInfo[24] = undefined
                    // val.classInfo[25] = undefined
                    // val.classInfo[26] = val.classInfo.color
                    val.classInfo.color = undefined
                    result.push(val.classInfo)
                }
            })
            let workbook = XLSX.utils.book_new()
            let sheet = XLSX.utils.json_to_sheet(result)
            XLSX.utils.book_append_sheet(workbook, sheet, 'Sheet1')
            XLSX.writeFile(workbook, 'time-table.xlsx');
        },
		saveToLocal() {
            let result = [this.rawTitle]
            this.chosenClasses.forEach(val => {
                if(val) {
                    val.classInfo.start = undefined // when get color from classInfo[26] will get classInfo.start
                    val.classInfo.end = undefined
                    // val.classInfo[24] = undefined
                    // val.classInfo[25] = undefined
                    // val.classInfo[26] = val.classInfo.color
                    val.classInfo.color = undefined
                    result.push(val.classInfo)
                }
            })
            localStorage.setItem('timeTable', JSON.stringify(result))
            this.hasLocalTimeTable = true
            this.$toast.success('Đã lưu vào bộ nhớ của Browser')
        },
		isClassesOverlapWeek(layerClasses) {
			if(layerClasses.length > 1) {
				let classes = layerClasses.map(val => this.chosenClasses[val].classInfo)
				let weeksMap = {}
				for(let i = 0; i < classes.length; i++) {
					let weeks = this.getRowWeeks(classes[i])
					for(let j = 0; j < weeks.length; j++) {
						if(weeksMap[weeks[j]]) {
							return true
						}
						weeksMap[weeks[j]] = true
					}
				}
			}
			return false
		},
		getRowWeeks(data) {
			let weekString = this.getRowInfo(data, 'week')

			if (this.weeksMap[weekString]) {
				return this.weeksMap[weekString]
			}
			let splitByComma = weekString.split(',')
			let ans = []
			splitByComma.forEach(val => {
				if (val.indexOf('-') == -1) {
					ans.push(parseInt(val))
					return
				}

				let range = val.split('-')
				if (range.length != 2 || parseInt(range[0]) > parseInt(range[1])) return
				for(let i = parseInt(range[0]); i <= parseInt(range[1]); i++) {
					ans.push(i)
				}
			})

			this.weeksMap[weekString] = ans
			return ans
		},
	}
  }