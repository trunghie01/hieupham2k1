import XLSX from 'xlsx';

export default {
	data() {
		return {
			isLoading: false,
			subjectTree: {},
		}
	},
	methods: {
		uploadFile(file) {
            this.fileList = [file]
            this.fileReader(file.raw)
        },
		fileReader(oFile) {
            var reader = new FileReader();
            this.isLoading = true
            this.$loading()

            reader.onload = (e) => {
                var data = e.target.result;
                data = new Uint8Array(data);
                try {
                    var workbook = XLSX.read(data, {type: 'array'});
                } catch(e){
                    console.log(e)
                    this.isLoading = false
                    this.$loading().close()
                    this.$toast.error('Xảy ra lỗi khi đọc file, xin hãy kiểm tra lại.')
                    return
                }
                var result = {};
                workbook.SheetNames.forEach(function (sheetName) {
                    var roa = XLSX.utils.sheet_to_json(workbook.Sheets[sheetName], {header: 1});
                    if (roa.length) result[sheetName] = roa;
                });

                this.rawTable = []
                for(let sheet in result){
                    this.rawTable = [...this.rawTable, ...result[sheet]]
                }
                this.initTable()

                this.isLoading = false
                this.$loading().close()
                if(!this.$swal.isVisible()) this.$toast.success('Đã tải lên TKB')

                if(this.isImported) {
					this.importFile()
					this.fileList = []
				}
            }
            try{
                reader.readAsArrayBuffer(oFile);
            } catch(e){
                this.isLoading = false
                this.$loading().close()
                this.$toast.error('Xảy ra lỗi khi đọc file')
            }
        },
		initSubjectTree() {
			function removeAccents(str) {
                return str.normalize('NFD').replace(/[\u0300-\u036f]/g, '').replace(/đ/g, 'd').replace(/Đ/g, 'D');
            }

			let hasInit = {}
			for(let i = 0; i < this.rawTable.length; i++){
				let subjectName = removeAccents(this.getRowInfo(this.rawTable[i], 'subjectName').toUpperCase())
				if(hasInit[subjectName]) continue
				hasInit[subjectName] = true

				for(let i1 = 0; i1 < subjectName.length; i1++) {
					let pointer = this.subjectTree
					for(let j = i1; j < subjectName.length; j++) {
						let char = subjectName.charAt(j)
						if(!pointer[char]) {
							pointer[char] = {
								rows: [],
							}
						}
						pointer[char].rows.push(this.rawTable[i])
						pointer = pointer[char]
					}
				}
			}
			console.log(this.subjectTree)
		},
        findTitle(sheet) {
            const flip = (array) => {
                let result = {}
                let len = array.length
                for(let i = 0; i < len; i++) {
                    result[array[i]] = i
                }
                return result
            }

            let  i = 0
            let firstTitle = Object.values(this.titleTrans)[0]
            while(i < 1000){
                let row = sheet[i++]
                if(!row) return this.$toast.error('Tên cột mặc định đang khác với tên cột trong TKB , xin hãy sửa trong phần "Hướng dẫn"',
                    { duration: 0 })
                if(row.indexOf(firstTitle) != -1){
                    this.rawTitle = row
                    this.title = flip(row)
                    if(this.isImported) this.rawTable.splice(0, i) // delete title from raw table to get class info only
                    break
                }
            }
        },
		filterSubject() {
            if(this.rawTable.length == 0) {
				return this.$toast.error('Chưa có TKB')
			}
            else if(this.subjectCodes.length == 0 && !this.isImported) {
				return this.$toast.error('Chưa nhập mã học phần nào')
			}

            this.jsonTable = {}
            this.tableTime = []
            this.layer = {}

            let exsistSubjectCode = []
            let len = this.rawTable.length

			if (this.isImported && len > 1000) {
				return this.$toast.error('Quá nhiều lớp, có phải bạn đã import TKB dự kiến? Đây không phải nút upload TKB dự kiến!', { timer: 5000 })
			}

            for(let i = 0; i < len; i++) {

                let row = this.rawTable[i]
                // if(row.length < this.rawTitle.length) { // must comment be cause exist row.length < this.rawTitle.length but row is a subject
				// 	continue
				// }

                let rowProgramName = this.getRowInfo(row, 'programName')
                let sameProgram = this.programName == "All" || rowProgramName && rowProgramName.toLowerCase() == this.programName.toLowerCase()
                let indexOfRowSubjectCode = this.subjectCodes.indexOf(this.getRowInfo(row, 'subjectCode'))

                if(indexOfRowSubjectCode != -1 && sameProgram || this.isImported) {

					let rowTime = this.getRowInfo(row, 'time')
                    if(!rowTime){ // must not return because rowTime can be empty
						console.error('Cant find rowTime of this row', row)
                        // this.$toast.error(`Không tìm thấy cột "${this.titleTrans['time']}" hoặc "${this.titleTrans['time']}" không có giá trị, xin hãy kiểm tra lại file Excel và sửa trong phần "Hướng dẫn"`)
						continue
                    }

					exsistSubjectCode[indexOfRowSubjectCode] = true

                    let time = rowTime.split('-')
                    row.start = time[0]
                    row.end = time[1]

                    if(this.tableTime.indexOf(time[0]) == -1) this.tableTime.push(time[0])
                    if(this.tableTime.indexOf(time[1]) == -1) this.tableTime.push(time[1])

                    this.addToJsonTable(row)
                }
            }

            let notExistSubjectCode = ''
            for(let i in this.subjectCodes){
                if(!exsistSubjectCode[i]) {
                    notExistSubjectCode += `<b>${this.subjectCodes[i]}: ${this.subjectNameMap[this.subjectCodes[i]] ? this.subjectNameMap[this.subjectCodes[i]] : 'Không tìm thấy'}</b>, `
                }
            }

            if(notExistSubjectCode.length > 0){
                this.$refs['not-exist-code'].innerHTML = `Các mã học phần không có lớp: ${notExistSubjectCode} (${this.programName})`
            }

            this.tableTime.sort()

			setTimeout(() => {
				this.$toast.success('Đã xong!')
			}, 0)
        },
		addToJsonTable(row) {
            let rowSubjectCode = this.getRowInfo(row, 'subjectCode')
            if(!this.jsonTable[rowSubjectCode]){
                this.jsonTable[rowSubjectCode] = {}
            }
            let rowClassCode = this.getRowInfo(row, 'classCode')
            if(!this.jsonTable[rowSubjectCode][rowClassCode]){
                this.jsonTable[rowSubjectCode][rowClassCode] = []
            }
            this.jsonTable[rowSubjectCode][rowClassCode].push(row)

            if(!this.jsonTable[rowSubjectCode][rowClassCode].classType){
                this.jsonTable[rowSubjectCode][rowClassCode].classType = this.getRowInfo(row, 'classType')
            }

            if(!this.jsonTable[rowSubjectCode].subjectName){
                this.jsonTable[rowSubjectCode].subjectName = this.getRowInfo(row, 'subjectName')
            }

            if(!this.jsonTable[rowSubjectCode].creditsInfo){
                this.jsonTable[rowSubjectCode].creditsInfo = this.getRowInfo(row, 'creditsInfo')
            }
        },
		toggleChangeTitle() {
            this.changeTitle = !this.changeTitle;
            this.$swal.close()
            if(!this.changeTitle){
                this.findTitle(this.rawTable)
            }
        },
		hasExpectedTimeTableInStorage() {
			return localStorage.getItem('expectedTimeTable') ? true : false
		},
		saveExpectedTimeTableToStorage() {
			localStorage.setItem('expectedTimeTable', JSON.stringify(this.rawTable))
			localStorage.setItem('expectedTimeTableName', this.fileList[0].name)

			this.$toast.success('Đã lưu TKB dự kiến vào browser, lần sau bạn không cần upload nữa.')
		},
		loadExpectedTimeTableFromStorage() {
			this.rawTable = JSON.parse(localStorage.getItem('expectedTimeTable')) ?? []
			if(!this.rawTable) {
				this.rawTable = []
				return this.$toast.error('Không có TKB dự kiến nào lưu trong browser!')
			}

			this.fileList = [{
				name: this.expectedTimeTableName,
			}]

			this.initTable()
			this.$toast.success('Đã load TKB dự kiến, giờ bạn đã có thể chọn lớp.')
		},
	}
  }