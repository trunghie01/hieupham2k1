import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
	{
		path: '/',
		name: 'Home',
		component: () => import('../views/Navs/Home.vue'),
	},
	{
		path: '/',
		name: 'Projects',
		component: () => import('../views/Navs/Projects.vue'),
		children: [
			{
				path: 'time-table',
				name: 'TimeTable',
				describe: `
					A time table scheduling tool.
				`,
				component: () => import('../views/TimeTable/Master.vue'),
			},
			{
				path: 'drl',
				name: 'DRL',
				describe: `
					A activity grading tool.
				`,
				component: () => import('../views/DRL/Master.vue'),
				children: [
					{
						path: '/',
						name: 'DRL',
						component: () => import('../views/DRL/DRL.vue'),
					},
					{
						path: 'how-to',
						name: 'DRLHowTo',
						component: () => import('../views/DRL/HowTo.vue'),
					},
				],
			},
			{
				path: 'tanks',
				name: 'Tanks',
				describe: `
					Realtime shooting game.
				`,
				component: () => import('../views/Tanks/Master.vue'),
				children: [
					{
						path: '/',
						name: 'Login',
						component: () => import('../views/Tanks/Login.vue'),
					},
					{
						path: ':roomId/:tankId',
						name: 'Room',
						component: () => import('../views/Tanks/Room.vue'),
					},
					{
						path: ':roomId/:tankId/delete',
						name: 'Delete',
						component: () => import('../views/Tanks/Delete.vue'),
					},
				],
			},
			{
				path: 'xo',
				name: 'XO',
				describe: `
					Realtime tic tac toe.
				`,
				component: () => import('../views/XO/Master.vue'),
				children: [
					{
						path: '/',
						name: 'XOLogin',
						component: () => import('../views/XO/Login.vue'),
					},
					{
						path: ':roomId/:userId',
						name: 'XORoom',
						component: () => import('../views/XO/Room.vue'),
					},
				],
			},
			{
				path: 'lotto',
				name: 'Lotto',
				describe: `
					Scrape data from lotto web site.
				`,
				component: () => import('../views/Lotto/Master.vue'),
			},
			{
				path: 'cake',
				name: 'Cake',
				describe: `
					A cake with candles to blow virtually.
				`,
				component: () => import('../views/Cake/Master.vue'),
			},
			{
				path: 'eye-control',
				name: 'EyeControl',
				describe: `
					Use AI to detect face, eye then control the pointer.
				`,
				component: () => import('../views/EyeControl/Master.vue'),
			},
			{
				path: 'heartcrush',
				name: 'HeartCrush',
				describe: `
					A pipe connecting game.
				`,
				component: () => import('../views/HeartCrush/Master.vue'),
			},
			{
				path: 'cats',
				name: 'Cats',
				describe: `
					Get cat pictures from public API.
				`,
				component: () => import('../views/Cats/Master.vue'),
			},
		],
	},
]

const router = new VueRouter({
	routes,
	mode: 'history',
})

export default router
